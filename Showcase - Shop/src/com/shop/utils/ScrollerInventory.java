package com.shop.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.shop.shop.Item;
import com.shop.shop.Shop;
import com.shop.shop.ShopManager;

import net.md_5.bungee.api.ChatColor;

public class ScrollerInventory implements Listener {

	public ArrayList<Inventory> pages = new ArrayList<Inventory>();
	public UUID id;
	public int currpage = 0;
	public static HashMap<UUID, ScrollerInventory> users = new HashMap<UUID, ScrollerInventory>();
	public static final String nextPageName = "§aPróxima página";
	public static final String previousPageName = "§cPágina anterior";
	String name;
	private ItemStack main;
	private ItemStack nextpage;

	public String getName() {
		return name;
	}

	public ScrollerInventory(List<ItemStack> items, String name, Player player) {
		this.id = UUID.randomUUID();

		Inventory page = getBlankPage(name);
		Integer slot = 10;
		for (int i = 0; i < items.size(); i++) {
			if (slot == 44 /* 36 */) {
				this.pages.add(page);
				page = getBlankPage(name);
				slot = 10;
				page.setItem(slot, items.get(i));
				// if (slot == 16 || slot == 25 || slot == 34 || slot == 43) {
				// slot += 3;
				// } else {
				// slot += 1;
				// }
			} else {
				page.setItem(slot, items.get(i));
				if (slot == 16 || slot == 25 || slot == 34) {
					slot += 3;
				} else {
					slot += 1;
				}
			}
		}
		this.name = name;
		this.pages.add(page);

		player.openInventory(this.pages.get(this.currpage));
		users.put(player.getUniqueId(), this);
	}

	private Inventory getBlankPage(String name) {
		Inventory page = Bukkit.createInventory(null, 54, name + " §8- Página " + this.currpage);

		this.nextpage = new ItemStack(Material.ARROW, 1);
		ItemMeta meta = nextpage.getItemMeta();
		meta.setDisplayName(nextPageName);
		nextpage.setItemMeta(meta);

		this.main = new ItemStack(Material.ARROW, 1);
		meta = main.getItemMeta();
		meta.setDisplayName(previousPageName);
		main.setItemMeta(meta);

		// page.setItem(45, main);
		page.setItem(53, nextpage);
		return page;
	}

	public ScrollerInventory(InventoryClickEvent event, ShopManager shopManager) {

		Player player = (Player) event.getWhoClicked();

		if (event.getInventory().getName().startsWith("Loja")) {

			this.nextpage = new ItemStack(Material.ARROW, 1);
			ItemMeta meta = nextpage.getItemMeta();
			meta.setDisplayName(nextPageName);
			nextpage.setItemMeta(meta);

			this.main = new ItemStack(Material.ARROW, 1);
			meta = main.getItemMeta();
			meta.setDisplayName(previousPageName);
			main.setItemMeta(meta);

			if (event.getClick() == ClickType.NUMBER_KEY) {
				event.setCancelled(true);
			}

			if (event.getCurrentItem() != null) {
				event.setCancelled(true);

				ScrollerInventory inv = ScrollerInventory.users.get(player.getUniqueId());

				if (event.getCurrentItem().hasItemMeta()) {
					String displayName = event.getCurrentItem().getItemMeta().getDisplayName();

					if (displayName != null) {

						if (displayName.equalsIgnoreCase(previousPageName)) {
							int pag = inv.currpage;
							if ((pag - 1) < 0) {
							} else {
								inv.currpage -= 1;
								if (inv.pages.get(inv.currpage) != null) {

									Inventory invs = Bukkit.createInventory(null, 6 * 9, inv.getName() + " §8- Página " + inv.currpage + "");

									invs.setContents(inv.pages.get(inv.currpage).getContents());

									if ((inv.currpage - 1) < 0) {
										invs.setItem(45, new ItemStack(Material.AIR));
									} else {
										invs.setItem(45, main);
									}
									if (inv.currpage >= inv.pages.size() - 1) {
										invs.setItem(53, new ItemStack(Material.AIR));
									} else {
										invs.setItem(53, nextpage);
									}

									player.openInventory(invs);
								}
							}
						}

						if (displayName.equalsIgnoreCase(nextPageName)) {
							if (inv.currpage >= inv.pages.size() - 1) {
							} else {
								inv.currpage += 1;

								Inventory invs = Bukkit.createInventory(null, 6 * 9, inv.getName() + " §8- Página " + inv.currpage + "");

								invs.setContents(inv.pages.get(inv.currpage).getContents());

								if ((inv.currpage - 1) < 0) {
									invs.setItem(45, new ItemStack(Material.AIR));
								} else {
									invs.setItem(45, main);
								}

								if (inv.currpage >= inv.pages.size() - 1) {
									invs.setItem(53, new ItemStack(Material.AIR));
								} else {
									invs.setItem(53, nextpage);
								}

								player.openInventory(invs);
							}
						}
					}

					if (event.getCurrentItem().getItemMeta().hasLore()) {
						ItemStack crItem = event.getCurrentItem().clone();
						ItemMeta im = crItem.getItemMeta();
						List<String> lore = new ArrayList<String>();
						for (String l : im.getLore()) {
							String dl = ChatColor.stripColor(l);
							if (dl.toLowerCase().contains("compra: ") || dl.toLowerCase().contains("venda: ")) {
							} else {
								lore.add(l);
							}
						}
						im.setLore(lore);
						crItem.setItemMeta(im);

						Item item = null;

						for (Shop i : shopManager.getShops()) {
							Item it = i.getItems().stream().filter(i2 -> crItem.isSimilar(i2.getItem())).findAny().orElse(null);
							if (it != null) {
								item = it;
								break;
							}
						}

						if (item != null) {
							player.closeInventory();
							shopManager.getShopListener().buy(player, item);
						}
					}
				}
				if (ScrollerInventory.previousPageName != null) {
					event.setCancelled(true);
				}
			}
		}
	}
}
