package com.shop;

import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.shop.shop.ShopManager;
import com.shop.shop.customevent.UpdateTime;

import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin {

	private static Main INSTANCE;
	private ShopManager shopManager;
	private Economy economy;

	public static Main getInstance() {
		return INSTANCE;
	}

	@Override
	public void onEnable() {
		INSTANCE = this;

		setupEconomy();

		this.shopManager = new ShopManager();
		UpdateTime.runAsync();
	}

	private boolean setupEconomy() {
		RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager()
				.getRegistration(net.milkbowl.vault.economy.Economy.class);
		if (economyProvider != null) {
			economy = economyProvider.getProvider();
		}
		return economy != null;
	}

	public ShopManager getShopManager() {
		return this.shopManager;
	}

	public Economy getEconomy() {
		return this.economy;
	}
}
