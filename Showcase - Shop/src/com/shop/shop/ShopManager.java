package com.shop.shop;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.craftbukkit.v1_10_R1.CraftServer;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.shop.Main;
import com.shop.shop.category.CategoryManager;
import com.shop.shop.commands.CommandShop;
import com.shop.shop.storage.ShopSQLStorage;
import com.shop.shop.storage.ShopStorage;

public class ShopManager {

	private CategoryManager categoryManager;

	private ShopStorage storage;
	private Set<Shop> shops;

	private ShopListener shopListener;

	public ShopManager() {
		this.categoryManager = new CategoryManager();

		this.storage = new ShopSQLStorage();
		this.shops = storage.download();

		registerCommand(new CommandShop(this));

		this.shopListener = new ShopListener(this);
		Bukkit.getPluginManager().registerEvents(shopListener, Main.getInstance());
	}

	public void registerCommand(Command command) {
		((CraftServer) Bukkit.getServer()).getCommandMap().register(command.getName(), command);
	}

	public void addItemOnCategory(int category_id, ItemStack item, double price_buy, int limit) {
		Item i = new Item(item, limit, price_buy);
		Shop shop = getShop(category_id);

		if (shop != null) {
			shop.getItems().add(i);
			update(shop);
		} else {
			Shop create = storage.create(category_id);
			create.getItems().add(i);
			shops.add(create);
			update(create);
		}
	}

	public void removeItemOnCategory(int category_id, ItemStack item) {
		Shop shop = getShop(category_id);

		if (shop != null) {
			Item it = shop.getItems().stream().filter(i -> i.getItem().isSimilar(item)).findAny().orElse(null);
			if (it != null) {
				shop.getItems().remove(it);
				update(shop);
			}
		}
	}

	public void removeItemOnCategory(int category_id, Item item) {
		Shop shop = getShop(category_id);

		if (shop != null) {
			shop.getItems().remove(item);
			update(shop);
		}
	}

	public void deleteShop(int category_id) {
		Shop shop = getShop(category_id);

		if (shop != null) {
			storage.delete(shop.getCategory());
			shops.remove(shop);
		}
	}

	public void update(Shop shop) {
		new BukkitRunnable() {

			@Override
			public void run() {
				storage.update(shop);
			}
		}.runTaskAsynchronously(Main.getInstance());
	}

	public CategoryManager getCategoryManager() {
		return this.categoryManager;
	}

	public ShopStorage getStorage() {
		return this.storage;
	}

	public Shop getShop(int category_id) {
		return shops.stream().filter(i -> i.getCategory() == category_id).findAny().orElse(null);
	}

	public Set<Shop> getShops() {
		return this.shops;
	}

	public ShopListener getShopListener() {
		return this.shopListener;
	}
}
