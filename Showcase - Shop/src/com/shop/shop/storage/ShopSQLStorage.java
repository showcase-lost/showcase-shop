package com.shop.shop.storage;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.shop.shop.Item;
import com.shop.shop.Shop;

public class ShopSQLStorage implements ShopStorage {

	@Override
	public Set<Shop> download() {
		Set<Shop> shops = new HashSet<Shop>();

		return shops;
	}

	@Override
	public Shop create(int category_id) {

		return new Shop(category_id, new ArrayList<Item>());
	}

	@Override
	public void delete(int category_id) {
	}

	@Override
	public void update(Shop shop) {
	}
}
