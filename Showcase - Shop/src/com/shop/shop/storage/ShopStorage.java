package com.shop.shop.storage;

import java.util.Set;

import com.shop.shop.Shop;

public interface ShopStorage {

	public Set<Shop> download();

	public Shop create(int category_id);

	public void delete(int category_id);

	public void update(Shop shop);

}
