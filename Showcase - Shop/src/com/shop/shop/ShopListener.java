package com.shop.shop;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import com.shop.Main;
import com.shop.shop.customevent.BuyItemEvent;
import com.shop.shop.customevent.UpdateTime;
import com.shop.utils.ScrollerInventory;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_10_R1.NBTTagCompound;

public class ShopListener implements Listener {

	private ShopManager shopManager;

	private Set<String> needInsert;
	private Map<String, Integer> buyAmount;
	private Map<String, Item> buyItem;
	private Map<String, Long> expireBuy;

	private DecimalFormat df;

	public ShopListener(ShopManager shopManager) {
		this.shopManager = shopManager;

		this.needInsert = new HashSet<String>();
		this.buyAmount = new HashMap<String, Integer>();
		this.buyItem = new HashMap<String, Item>();
		this.expireBuy = new HashMap<String, Long>();

		this.df = new DecimalFormat("###,###,###,###,###.##");
	}

	@EventHandler
	public void onTime(UpdateTime event) {

		if (event.isAsync()) {
			Set<Entry<String, Long>> collect = expireBuy.entrySet().stream()
					.filter(i -> System.currentTimeMillis() > i.getValue()).collect(Collectors.toSet());

			collect.forEach(i -> {
				Player player = Bukkit.getPlayer(i.getKey());

				needInsert.remove(i.getKey());
				buyAmount.remove(i.getKey());
				buyItem.remove(i.getKey());
				expireBuy.remove(i.getKey());
				if (player != null) {
					player.sendMessage("");
					player.sendMessage("§7Compra cancelada, tempo expirado.");
					player.sendMessage("");
				}
			});
		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent event) {
		new ScrollerInventory(event, shopManager);
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();

		if (buyItem.containsKey(player.getName())) {
			needInsert.remove(player.getName());
			buyAmount.remove(player.getName());
			buyItem.remove(player.getName());
			expireBuy.remove(player.getName());

			player.sendMessage("");
			player.sendMessage("§7Compra cancelada!");
			player.sendMessage("");
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();

		if (needInsert.contains(player.getName())) {
			event.setCancelled(true);
			int amount = 0;
			try {
				amount = Integer.parseInt(message);
			} catch (NumberFormatException e) {
				player.sendMessage("");
				TextComponent m1 = new TextComponent("§aAQUI");
				m1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
						new ComponentBuilder("§eClique para cancelar!").create()));
				m1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/shop cancelar"));
				player.spigot().sendMessage(new TextComponent("§7Você deve utilizar apenas números! Cancele clicando "),
						m1);
				player.sendMessage("");
				return;
			}

			Item item = buyItem.get(player.getName());

			if (amount > item.getLimit() && item.getLimit() > 0) {
				player.sendMessage("");
				TextComponent m1 = new TextComponent("§aAQUI");
				m1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
						new ComponentBuilder("§eClique para cancelar!").create()));
				m1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/shop cancelar"));
				player.spigot().sendMessage(
						new TextComponent("§7Limite de compra é de " + item.getLimit() + "! Cancele clicando "), m1);
				player.sendMessage("");
				return;
			}

			if (amount <= 0) {
				player.sendMessage("");
				TextComponent m1 = new TextComponent("§aAQUI");
				m1.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
						new ComponentBuilder("§eClique para cancelar!").create()));
				m1.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/shop cancelar"));
				player.spigot()
						.sendMessage(new TextComponent("§7O mínimo para compra é de zero (0)! Cancele clicando "), m1);
				player.sendMessage("");
				return;
			}

			net.minecraft.server.v1_10_R1.ItemStack nmsItemStack = org.bukkit.craftbukkit.v1_10_R1.inventory.CraftItemStack
					.asNMSCopy(item.getItem());
			NBTTagCompound compound = new NBTTagCompound();
			nmsItemStack.save(compound);
			String json = compound.toString();
			BaseComponent[] itemHover = new BaseComponent[] { new TextComponent(json) };

			TextComponent m1 = new TextComponent("§7Deseja realmente comprar §a" + amount + "§7 ");
			TextComponent m2 = new TextComponent("§7" + item.getItem().getType() + "?");
			m2.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_ITEM, itemHover));

			double price = item.getPriceBuy() * amount;
			TextComponent m22 = new TextComponent("§7 Por §a" + df.format(price) + " coins.");

			TextComponent m3 = new TextComponent("§7Clique para ");

			TextComponent mA = new TextComponent("§a§lCOMPRAR");
			mA.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/shop buy"));
			TextComponent mR = new TextComponent("§c§lCANCELAR");
			mR.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/shop cancelar"));

			player.sendMessage("");
			player.spigot().sendMessage(m1, m2, m22);
			player.spigot().sendMessage(m3, mA);
			player.spigot().sendMessage(m3, mR);

			if (hasDiscount(player)) {

				double discount = price - price * getDiscount(player);
				TextComponent mDsc = new TextComponent(
						"§7Você possui desconto, novo preço por §a" + df.format(discount));
				player.spigot().sendMessage(mDsc);
			}

			player.sendMessage("");

			buyAmount.put(player.getName(), amount);
			needInsert.remove(player.getName());
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommand(PlayerCommandPreprocessEvent event) {
		Player player = event.getPlayer();
		String message = event.getMessage();

		if (message.equalsIgnoreCase("/shop cancelar")) {
			event.setCancelled(true);

			if (!buyItem.containsKey(player.getName())) {
				return;
			}

			needInsert.remove(player.getName());
			buyAmount.remove(player.getName());
			buyItem.remove(player.getName());
			expireBuy.remove(player.getName());

			player.sendMessage("");
			player.sendMessage("§7Compra cancelada!");
			player.sendMessage("");
			return;
		}
		if (message.equalsIgnoreCase("/shop buy")) {
			event.setCancelled(true);

			if (!buyAmount.containsKey(player.getName()) || !buyItem.containsKey(player.getName())) {
				return;
			}

			double amount = buyAmount.get(player.getName());
			Item item = buyItem.get(player.getName());
			double price = item.getPriceBuy() * amount;

			if (hasDiscount(player)) {
				price = price - price * getDiscount(player);
			}

			if (!Main.getInstance().getEconomy().has(player, price)) {
				player.sendMessage("");
				player.sendMessage("§cVocê não possui coins suficientes!");

				needInsert.remove(player.getName());
				buyAmount.remove(player.getName());
				buyItem.remove(player.getName());
				expireBuy.remove(player.getName());

				player.sendMessage("§7Compra cancelada!");
				player.sendMessage("");
				return;
			}

			Main.getInstance().getEconomy().withdrawPlayer(player, price);

			ItemStack item2 = item.getItem();
			item2.setAmount(1);

			while (amount > 0) {
				if (player.getInventory().firstEmpty() < 0) {
					player.getWorld().dropItem(player.getLocation(), item2);
					amount -= 1;
				} else {
					player.getInventory().addItem(item2);
					amount -= 1;
				}
			}
			needInsert.remove(player.getName());
			buyAmount.remove(player.getName());
			buyItem.remove(player.getName());
			expireBuy.remove(player.getName());
			player.sendMessage("");
			player.sendMessage("§7Compra efetuada com sucesso!");
			player.sendMessage("");

			Bukkit.getPluginManager().callEvent(new BuyItemEvent(player, item2, (int) amount));
			return;
		}
	}

	public void buy(Player player, Item item) {
		if (buyItem.containsKey(player.getName())) {
			player.sendMessage("");
			player.sendMessage("§7Você já está fazendo uma compra!");
			player.sendMessage("");
			return;
		}

		needInsert.add(player.getName());
		buyItem.put(player.getName(), item);
		expireBuy.put(player.getName(), System.currentTimeMillis() + TimeUnit.SECONDS.toMillis(120));

		player.sendMessage("");
		TextComponent m1 = new TextComponent("§7Insira a quantia de itens que deseja comprar.");
		m1.setHoverEvent(
				new HoverEvent(Action.SHOW_TEXT, new ComponentBuilder("§eBasta digitar o número no chat!").create()));
		player.spigot().sendMessage(m1);
		player.sendMessage("");
	}

	public double getDiscount(Player player) {
		if (player.hasPermission("shop.desconto.40")) {
			return 0.4;
		} else if (player.hasPermission("shop.desconto.35")) {
			return 0.35;
		} else if (player.hasPermission("shop.desconto.30")) {
			return 0.3;
		} else if (player.hasPermission("shop.desconto.25")) {
			return 0.25;
		} else if (player.hasPermission("shop.desconto.20")) {
			return 0.2;
		} else if (player.hasPermission("shop.desconto.15")) {
			return 0.15;
		} else if (player.hasPermission("shop.desconto.10")) {
			return 0.1;
		} else if (player.hasPermission("shop.desconto.5")) {
			return 0.05;
		}
		return 0;
	}

	public boolean hasDiscount(Player player) {
		return getDiscount(player) > 0.05;
	}

}
