package com.shop.shop.commands;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.shop.shop.Item;
import com.shop.shop.Shop;
import com.shop.shop.ShopManager;
import com.shop.shop.category.Category;
import com.shop.utils.ScrollerInventory;

import net.md_5.bungee.api.ChatColor;

public class CommandShop extends Command {

	private ShopManager shopManager;
	private DecimalFormat df;

	public CommandShop(ShopManager shopManager) {
		super("shop");
		this.shopManager = shopManager;
		this.df = new DecimalFormat("###,###,###,###,###.##");
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean execute(CommandSender sender, String arg1, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;

		if (!player.hasPermission("shop.admin")) {
			player.sendMessage("§cSem permissão!");
			return false;
		}

		if (args.length == 0) {
			player.sendMessage("§aComandos disponíveis:");
			player.sendMessage("§a * §7/" + getName() + " criar <categoria>");
			player.sendMessage("§a * §7/" + getName() + " deletar <categoria>");
			player.sendMessage("§a * §7/" + getName() + " abrir <categoria>");

			player.sendMessage("§a * §7/" + getName() + " <categoria> add <preço compra> <limite por compra>");
			player.sendMessage("§a * §7/" + getName() + " <categoria> remove <id>");
			return false;
		} else if (args.length >= 1) {

			if (args[0].equalsIgnoreCase("abrir")) {
				if (args.length == 1) {
					player.sendMessage("§a * §7/" + getName() + " abrir <categoria>");
					return false;
				}

				Category category = shopManager.getCategoryManager().getCategory(ChatColor.stripColor(args[1]));
				if (category != null) {
					Shop shop = shopManager.getShop(category.getId());

					List<ItemStack> items = new ArrayList<ItemStack>();
					if (shop != null && shop.getItems() != null) {
						shop.getItems().forEach(i -> {
							ItemStack item = i.getItem().clone();
							ItemMeta im = item.getItemMeta();

							List<String> lore = new ArrayList<String>();
							if (im.hasLore()) {
								lore.addAll(im.getLore());
							}
							lore.add("§aCompra: §f" + df.format(i.getPriceBuy()));

							im.setLore(lore);
							item.setItemMeta(im);

							items.add(item);
						});
					}

					new ScrollerInventory(items, "Loja", player);
				}
				return false;
			}

			if (args[0].equalsIgnoreCase("criar")) {
				if (args.length == 1) {
					player.sendMessage("§a * §7/" + getName() + " criar <categoria>");
					return false;
				}

				String category = args[1];

				Category ct = shopManager.getCategoryManager().getCategory(category);
				if (ct != null) {
					player.sendMessage("§eJá foi encontrado uma categoria com este nome!");
					return false;
				}

				shopManager.getCategoryManager().addCategory(category);
				player.sendMessage("§eCategoria criada com sucesso!");
				return false;
			}

			if (args[0].equalsIgnoreCase("deletar")) {
				if (args.length == 1) {
					player.sendMessage("§a * §7/" + getName() + " deletar <categoria>");
					return false;
				}

				String category = args[1];

				Category ct = shopManager.getCategoryManager().getCategory(category);
				if (ct == null) {
					player.sendMessage("§eEsta categoria não está cadastrada!");
					return false;
				}

				shopManager.getCategoryManager().remove(ct.getId());
				player.sendMessage("§eCategoria deletada com sucesso!");
				return false;
			}

			Category ct = shopManager.getCategoryManager().getCategory(args[0]);

			if (ct == null) {
				player.sendMessage("§eEsta categoria não está cadastrada!");
				return false;
			}

			if (args.length == 1) {
				player.sendMessage("§a * §7/" + getName() + " <categoria> add <preço compra> <limite por compra>");
				player.sendMessage("§a * §7/" + getName() + " <categoria> remove <id>");
				return false;
			} else if (args.length >= 2) {
				if (args[1].equalsIgnoreCase("add")) {
					if (args.length == 2) {
						player.sendMessage("§a * §7/" + getName() + " <categoria> add <preço compra>");
						return false;
					}

					double price_buy = 0;
					int limit = 0;

					try {
						price_buy = Double.parseDouble(args[2]);
						limit = Integer.parseInt(args[3]);
					} catch (NumberFormatException e) {
						player.sendMessage("§7Utilize apenas números!");
						return false;
					}

					if (price_buy <= 0) {
						player.sendMessage("§7O valor precisa ser maior que 0!");
						return false;
					}

					ItemStack item = player.getItemInHand();
					if (item == null || item.getType() == Material.AIR) {
						player.sendMessage("§7É necessário estar segurando o item a ser vendido!");
						return false;
					}

					shopManager.addItemOnCategory(ct.getId(), item, price_buy, limit);
					player.sendMessage("§cItem adicionado na categoria " + ct.getName() + "§c.");
					return false;
				}

				if (args[1].equalsIgnoreCase("remove")) {
					if (args.length == 2) {
						player.sendMessage("§a * §7/" + getName() + " <categoria> remove <id>");
						return false;
					}

					int id_item = 0;

					try {
						id_item = Integer.parseInt(args[2]);
					} catch (NumberFormatException e) {
						player.sendMessage("§7Utilize apenas números!");
						return false;
					}

					Shop shop = shopManager.getShop(ct.getId());

					List<Item> items = shop.getItems();
					if (items.size() == 0) {
						player.sendMessage("§7Não há itens para serem removidos!");
						return false;
					}

					if (items.size() < id_item) {
						player.sendMessage("§7Você precisa inserir um id menor que a quantia de itens adicionados!");
						return false;
					}

					Item item = items.get(id_item);

					if (item == null) {
						player.sendMessage("§7Item não encontrado!");
						return false;
					}

					shopManager.removeItemOnCategory(ct.getId(), item);

					player.sendMessage("§eItem removido com sucesso!");
					return false;
				}
			}
		}

		return false;
	}

}
