package com.shop.shop;

import java.util.ArrayList;
import java.util.List;

public class Shop {

	private int category_id;
	private List<Item> items;

	public Shop(int category_id, List<Item> items) {
		this.category_id = category_id;
		this.items = items;
	}

	public int getCategory() {
		return this.category_id;
	}

	public void setCategory(int category_id) {
		this.category_id = category_id;
	}

	public List<Item> getItems() {
		return items == null ? new ArrayList<Item>() : items;
	}

}
