package com.shop.shop.category;

import java.util.Set;

import com.shop.shop.category.storage.CategorySQLStorage;
import com.shop.shop.category.storage.CategoryStorage;

public class CategoryManager {

	private CategoryStorage storage;
	private Set<Category> categorys;

	public CategoryManager() {
		this.storage = new CategorySQLStorage();
		this.categorys = storage.download();
	}

	public void addCategory(String name) {
		Category category = storage.create(name);
		categorys.add(category);
	}

	public void remove(int id) {
		Category category = categorys.stream().filter(i -> i.getId() == id).findAny().orElse(null);
		if (category != null) {
			categorys.remove(category);
			storage.delete(id);
		}
	}

	public Category getCategory(String name) {
		return categorys.stream().filter(i -> i.getName().equalsIgnoreCase(name)).findAny().orElse(null);
	}

	public Category getCategory(int id) {
		return categorys.stream().filter(i -> i.getId() == id).findAny().orElse(null);
	}

	public Set<Category> getCategorys() {
		return this.categorys;
	}

	public CategoryStorage getStorage() {
		return this.storage;
	}

}
