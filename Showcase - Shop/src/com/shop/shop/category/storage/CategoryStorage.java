package com.shop.shop.category.storage;

import java.util.Set;

import com.shop.shop.category.Category;

public interface CategoryStorage {

	public Set<Category> download();

	public Category create(String name);

	public void delete(int id);

}
