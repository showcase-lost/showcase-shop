package com.shop.shop.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class BuyItemEvent extends Event {

	public final HandlerList handlerList = new HandlerList();

	private Player player;
	private ItemStack item;
	private int amount;

	public BuyItemEvent(Player player, ItemStack item, int amount) {
		this.player = player;
		this.item = item;
		this.amount = amount;
	}

	public Player getPlayer() {
		return this.player;
	}

	public ItemStack getItem() {
		return this.item;
	}

	public int getAmount() {
		return this.amount;
	}

	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
