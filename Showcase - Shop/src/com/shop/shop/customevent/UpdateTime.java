package com.shop.shop.customevent;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.scheduler.BukkitRunnable;

import com.shop.Main;

public class UpdateTime extends Event {

	public final HandlerList handlerList = new HandlerList();

	private boolean async;

	public UpdateTime(boolean async) {
		this.async = async;
	}

	public boolean isAsync() {
		return this.async;
	}

	private static final HandlerList handlers = new HandlerList();

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public static void runAsync() {
		new BukkitRunnable() {

			@Override
			public void run() {
				Bukkit.getPluginManager().callEvent(new UpdateTime(true));
			}
		}.runTaskTimerAsynchronously(Main.getInstance(), 20, 20);
	}

}
