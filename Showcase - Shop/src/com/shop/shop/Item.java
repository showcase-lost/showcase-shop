package com.shop.shop;

import org.bukkit.inventory.ItemStack;

import com.shop.utils.InventoryUtils;

public class Item {

	private String item;
	private int limit;
	private double priceBuy;

	public Item(ItemStack item, int limit, double priceBuy) {
		this.item = InventoryUtils.itemstackToString(item);
		this.limit = limit;
		this.priceBuy = priceBuy;
	}

	public ItemStack getItem() {
		return InventoryUtils.stringToItemStack(item);
	}

	public double getPriceBuy() {
		return this.priceBuy;
	}

	public int getLimit() {
		return this.limit;
	}

}
